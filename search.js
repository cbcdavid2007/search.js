import { Form, Button, Alert } from 'react-bootstrap';
import DoughnutChart from '../../components/DoughnutChart';
import { useState } from 'react'
import toNum from '../../helpers/toNum' 
import Head from 'next/head';
import Swal from 'sweetalert2';

export default function Home({data}) {	

	const countriesStats = data.countries_stat
	
	countriesStats.sort(function(a, b){
	    if(a.country_name < b.country_name) { return -1; }
	    if(a.country_name > b.country_name) { return 1; }
	    return 0;
	})

	console.log(data)
	console.log(countriesStats)

	const countryList = countriesStats.map((country)=>{
		return(			
			<option key={country.country_name} value={country.country_name}> {country.country_name} </option> 
		)
	})

	const [targetCountry, setTargetCountry] = useState('');
	const [name, setName] = useState('');
	const [criticals, setCriticals] = useState(0);
	const [deaths, setDeaths] = useState(0);
	const [recoveries, setRecoveries] = useState(0);

	function search(e){
		e.preventDefault();

		const match = countriesStats.find(country => country.country_name.toLowerCase() === targetCountry.toLowerCase())

		console.log(match)

		if(match){
			setName(match.country_name)
			setCriticals(toNum(match.serious_critical))
			setDeaths(toNum(match.deaths))
			setRecoveries(toNum(match.total_recovered))
		} else {
			console.log("Check your input.")
			// alert("Search failed. Check your input properly.")
			Swal.fire('Search failed', 'Check your input properly.', 'error')
		}
	}

	return (
		<>

			<Head>
				<title> Covid-19 Country Search </title>
			</Head>

			<Form onSubmit={ search }>
				<Form.Group controlId="country">
					<Form.Label>Country</Form.Label>
					<Form.Control as="select" 
						value={targetCountry}
						onChange={e=>setTargetCountry(e.target.value)} 
					>
					<option value={""}> Please select a country. </option> 					
					{countryList}
					</Form.Control>					
					<Form.Text className="text-muted">
						Get Covid-19 stats of searched for country.
					</Form.Text>
				</Form.Group>

				<Button variant="primary" type="submit">
				Submit
				</Button>
			</Form>

			{name !== '' ? 
				<>
					<h1> Country: {name} </h1>	
					<DoughnutChart criticals={criticals} deaths={deaths} recoveries={recoveries} />
				</>
				:
				<Alert variant = "info" className="mt-4">
					Search for a country to visualize its data
				</Alert>
			}

		</>
	)
}

export async function getStaticProps() { 
	const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
	"method": "GET",
	"headers": {
		"x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
		"x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
	  }
	})
	const data = await res.json()

	return {
		props: {
			data
		}
	}
}
